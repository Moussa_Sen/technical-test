package com.ekwateur.technicaltest.domain.mappers;


import com.ekwateur.technicaltest.domain.dtos.ClientDTO;
import com.ekwateur.technicaltest.domain.dtos.ParticulierDTO;
import com.ekwateur.technicaltest.domain.dtos.ProDTO;
import com.ekwateur.technicaltest.domain.entities.Client;
import com.ekwateur.technicaltest.domain.entities.Particulier;
import com.ekwateur.technicaltest.domain.entities.Pro;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.SubclassMapping;

@Mapper(componentModel = "spring")
public interface MyMapper {

    ClientDTO toClientDto(final Client client);

    @SubclassMapping(source = Particulier.class, target = Client.class)
    Client toClientData(ClientDTO clientDTO);

    ParticulierDTO toParticulierDto(final Particulier particulier);

    ProDTO toProDto(final Pro pro);

}
