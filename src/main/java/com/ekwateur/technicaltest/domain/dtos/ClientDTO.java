package com.ekwateur.technicaltest.domain.dtos;

import com.ekwateur.technicaltest.domain.enums.TypeEnergie;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
public class ClientDTO {

    private String id;
    private TypeEnergie typeEnergie;
}
