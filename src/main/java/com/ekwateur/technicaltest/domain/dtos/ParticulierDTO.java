package com.ekwateur.technicaltest.domain.dtos;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
public class ParticulierDTO extends ClientDTO{
    private String civilite;
    private String nom;
    private String prenom;
}
