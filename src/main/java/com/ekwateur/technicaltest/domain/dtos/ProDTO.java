package com.ekwateur.technicaltest.domain.dtos;

import com.ekwateur.technicaltest.domain.enums.TypeEnergie;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
public class ProDTO extends ClientDTO{

    private String siret;
    private String raison_sociale;
    private double ca;
}
