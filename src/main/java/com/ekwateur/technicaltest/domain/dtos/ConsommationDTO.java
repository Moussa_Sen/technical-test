package com.ekwateur.technicaltest.domain.dtos;

import lombok.Data;

@Data
public class ConsommationDTO {

    private String clientId;
    private double kwh;
}
