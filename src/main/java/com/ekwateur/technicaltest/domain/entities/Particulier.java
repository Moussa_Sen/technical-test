package com.ekwateur.technicaltest.domain.entities;

import lombok.*;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;


@Data
@NoArgsConstructor
@AllArgsConstructor

@Entity
@DiscriminatorValue("PARTICULIER")
@Builder
@EqualsAndHashCode(callSuper=true)
public class Particulier extends Client {

    private String civilite;
    private String nom;
    private String prenom;
}
