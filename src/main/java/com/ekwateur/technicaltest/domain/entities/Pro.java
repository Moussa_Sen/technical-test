package com.ekwateur.technicaltest.domain.entities;


import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;


@Data
@NoArgsConstructor
@AllArgsConstructor

@Entity
@DiscriminatorValue("PRO")
@Builder
@EqualsAndHashCode(callSuper=true)
public class Pro extends Client {

    private String siret;
    private String raison_sociale;
    private double ca;

}
