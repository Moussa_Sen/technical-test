package com.ekwateur.technicaltest.domain.entities;


import com.ekwateur.technicaltest.domain.enums.TypeEnergie;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)

@DiscriminatorColumn(name = "TYPE",length = 12,discriminatorType = DiscriminatorType.STRING)
public class Client {

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    @Enumerated(EnumType.STRING)
    private TypeEnergie typeEnergie;

}
