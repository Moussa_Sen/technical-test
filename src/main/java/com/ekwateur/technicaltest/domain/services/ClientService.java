package com.ekwateur.technicaltest.domain.services;

import com.ekwateur.technicaltest.domain.dtos.ClientDTO;
import com.ekwateur.technicaltest.domain.dtos.ParticulierDTO;
import com.ekwateur.technicaltest.domain.dtos.ProDTO;
import com.ekwateur.technicaltest.domain.entities.Client;
import com.ekwateur.technicaltest.domain.entities.Particulier;
import com.ekwateur.technicaltest.domain.entities.Pro;
import com.ekwateur.technicaltest.domain.mappers.MyMapper;
import com.ekwateur.technicaltest.runtime.handlers.errors.ResourceException;
import com.ekwateur.technicaltest.runtime.repository.ClientRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
//@AllArgsConstructor

public class ClientService {

    private ClientRepository clientRepository;

    private MyMapper myMapper;

    private final static int MINNUMBER =10000000;
    private final static int MAXNUMBER =99999999;
    private final static String PREFIXID ="EKW";


    public ClientService(ClientRepository clientRepository,MyMapper myMapper) {
        this.clientRepository = clientRepository;
        this.myMapper=myMapper;
    }

    public  List<ClientDTO> ClientDTOList() {

        List<Client> clients = this.clientRepository.findAll();

        return clients.stream()
                .map(cli -> myMapper.toClientDto((cli)))
                .collect(Collectors.toList());

    }

    public ClientDTO getClient(String clientId) {
        Client client=this.clientRepository.findById(clientId)
                .orElseThrow(()->new ResourceException(HttpStatus.NOT_FOUND,"Client inexistant"));

        return this.myMapper.toClientDto(client);
    }

    public ProDTO saveProClient(ProDTO proDTO) {
        Pro pro = new Pro();
        pro.setId( getRandomNumberUsingInts());

        pro.setSiret(proDTO.getSiret());
        pro.setRaison_sociale(proDTO.getRaison_sociale());
        pro.setCa(proDTO.getCa());
        pro.setTypeEnergie(proDTO.getTypeEnergie());

        return this.myMapper.toProDto(this.clientRepository.save(pro));

    }

    public ParticulierDTO saveParticulierClient(ParticulierDTO particulierDTO) {
        Particulier particulier = new Particulier();

        particulier.setId( getRandomNumberUsingInts());

        particulier.setCivilite(particulierDTO.getCivilite());
        particulier.setNom(particulierDTO.getNom());
        particulier.setPrenom(particulierDTO.getPrenom());
        particulier.setTypeEnergie(particulierDTO.getTypeEnergie());

        return myMapper.toParticulierDto(this.clientRepository.save(particulier));

    }
    public double calculer(String clientId, double kWh ) {

        Client client = clientRepository.findById(clientId)
                .orElseThrow(()->new ResourceException(HttpStatus.NOT_FOUND,"Client inexistant"));

        double prix=0;
        boolean isGaz = client.getTypeEnergie().equals("GAZ");
        if(client instanceof Pro) {
            Pro pro = (Pro) client;

            if (pro.getCa() > 1000000) {
                if (isGaz)
                    prix = 0.111 * kWh;
                else prix = 0.114 * kWh;
            } else {
                if (isGaz)
                    prix = 0.113 * kWh;
                else prix = 0.118 * kWh;
            }
        }
        //cas d'un client particulier
        else{

            if (isGaz)
                prix = 0.115 * kWh;
            else prix = 0.121 * kWh;
        }

        return prix;
    }

    public String getRandomNumberUsingInts() {
        Random random = new Random();
        return PREFIXID+random.ints(MINNUMBER, MAXNUMBER)
                .findFirst()
                .getAsInt();
    }

}
