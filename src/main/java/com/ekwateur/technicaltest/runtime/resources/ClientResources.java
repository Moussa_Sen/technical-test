package com.ekwateur.technicaltest.runtime.resources;

import com.ekwateur.technicaltest.domain.dtos.ClientDTO;
import com.ekwateur.technicaltest.domain.dtos.ConsommationDTO;
import com.ekwateur.technicaltest.domain.dtos.ParticulierDTO;
import com.ekwateur.technicaltest.domain.dtos.ProDTO;
import com.ekwateur.technicaltest.domain.entities.Client;
import com.ekwateur.technicaltest.domain.services.ClientService;
import com.ekwateur.technicaltest.runtime.handlers.errors.ResourceException;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value="/api/v1")
public class ClientResources {

    private final ClientService clientService;

    public ClientResources(ClientService clientService) {
        this.clientService = clientService;
    }

    @Operation(summary = "recuperer la liste des clients")
    @GetMapping("/clients")
    public List<ClientDTO> listClients()  {
        return this.clientService.ClientDTOList();
    }

    @Operation(summary = "recuperer un client grace à son identifiant")
    @GetMapping("/clients/{clientId}")
    public ClientDTO getClient(@PathVariable String clientId) {
        return this.clientService.getClient(clientId);
    }

    @Operation(summary = "ajouter un client pro")
    @PostMapping("/clients/pro")
    public ProDTO saveProClient (@RequestBody ProDTO proDTO)  {
        return this.clientService.saveProClient(proDTO);
    }

    @Operation(summary = "ajouter un client particulier")
    @PostMapping("/clients/particulier")
    public ParticulierDTO saveParticulierClient(@RequestBody ParticulierDTO particulierDTO) {
        return this.clientService.saveParticulierClient(particulierDTO);
    }
    @Operation(summary = "calculer la facture d'un client")
    @PostMapping("/clients/calculer")
    public double calculerEnergie(@RequestBody ConsommationDTO cons) {
        return this.clientService.calculer(cons.getClientId(),cons.getKwh());
    }

}
