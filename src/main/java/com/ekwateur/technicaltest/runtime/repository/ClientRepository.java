package com.ekwateur.technicaltest.runtime.repository;

import com.ekwateur.technicaltest.domain.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, String> {

}
