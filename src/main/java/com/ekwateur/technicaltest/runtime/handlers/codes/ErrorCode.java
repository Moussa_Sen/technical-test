package com.ekwateur.technicaltest.runtime.handlers.codes;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class ErrorCode {

    private String message;
    private int code;

}
