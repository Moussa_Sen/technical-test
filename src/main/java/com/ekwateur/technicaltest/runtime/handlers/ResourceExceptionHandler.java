package com.ekwateur.technicaltest.runtime.handlers;

import com.ekwateur.technicaltest.runtime.handlers.codes.ErrorCode;
import com.ekwateur.technicaltest.runtime.handlers.errors.ResourceException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ResourceExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ResourceException.class)
    protected ResponseEntity<Object> handleNotFound(ResourceException ex, WebRequest request) {

        return handleExceptionInternal(ex, new ErrorCode(ex.getReason(),ex.getRawStatusCode()),
                new HttpHeaders(), HttpStatus.valueOf(ex.getRawStatusCode()), request);
    }

}
